import java.util.ArrayList;

public class TrampolineMaze {
	static int getSteps(String input) {
		int steps = 0;
		ArrayList<Integer> offsets = new ArrayList<Integer>();
		
		String lines[] = input.split("\\r?\\n");
		for (int i = 0; i < lines.length; i++) {
			offsets.add(Integer.parseInt(lines[i]));
		}
		
		int i = 0;
		while (i >= 0 && i < offsets.size()) {
			int temp = i;
			i += offsets.get(i);
			if (offsets.get(temp) >= 3) {
				offsets.set(temp, offsets.get(temp) - 1);
			} else {
				offsets.set(temp, offsets.get(temp) + 1);
			}
			
			steps++;
		}
		
		return steps;
	}
}
