// Day 2
public class CorruptionChecksum {
	static int getChecksum(String spreadsheet) {
		int checksum = 0;
		
		String lines[] = spreadsheet.split("\\r?\\n");
		for (int i = 0; i < lines.length; i++) {
			String numbers[] = lines[i].split("\\s+");
			
			int min = Integer.parseInt(numbers[0]);
			int max = min;
			for (int j = 1; j < numbers.length; j++) {
				int num = Integer.parseInt(numbers[j]);
				if (num < min) {
					min = num;
				} else if (num > max) {
					max = num;
				}
			}
			
			checksum += max - min;
		}
		
		return checksum;
	}
	
	static int getEvenDivisorSum(String spreadsheet) {
		int checksum = 0;
		
		String lines[] = spreadsheet.split("\n");
		for (int i = 0; i < lines.length; i++) {
			String numbers[] = lines[i].split("\\s+");
			
			for (int j = 0; j < numbers.length; j++) {
				int num1 = Integer.parseInt(numbers[j]);
				
				for (int k = j + 1; k < numbers.length; k++) {
					int num2 = Integer.parseInt(numbers[k]);
					
					if (num1 % num2 == 0) {
						checksum += num1 / num2;
						break;
					} else if (num2 % num1 == 0) {
						checksum += num2 / num1;
						break;
					}
				}
			}
		}
		
		return checksum;
	}
}
