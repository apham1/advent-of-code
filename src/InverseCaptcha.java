// Day 1
public class InverseCaptcha {
	static int getSum(String captcha) {
		int sum = 0;
		
		for (int i = 0; i < captcha.length(); i++) {
			if (i < captcha.length() - 1) {
				if (captcha.charAt(i) == captcha.charAt(i+1)) {
					sum += Character.getNumericValue(captcha.charAt(i));
				}
			} else {
				if (captcha.charAt(i) == captcha.charAt(0)) {
					sum += Character.getNumericValue(captcha.charAt(i));
				}
			}
		}
		
		return sum;
	}
	
	static int getHalfwaySum(String captcha) {
		int sum = 0;
		
		for (int i = 0; i < captcha.length(); i++) {
			if (i < captcha.length() - 1) {
				if (i + (captcha.length() / 2) < captcha.length()) {
					if (captcha.charAt(i) == captcha.charAt(i + captcha.length() / 2)) {
						sum += Character.getNumericValue(captcha.charAt(i));
					}
				} else {
					if (captcha.charAt(i) == captcha.charAt(i - captcha.length() / 2)) {
						sum += Character.getNumericValue(captcha.charAt(i));
					}
				}
			}
		}
		
		return sum;
	}
}