import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.*;

public class App {
	public static void main(String[]args) throws IOException {
		System.out.println("Day 1: Inverse Captcha\n"
						 + "----------------------");
		String captcha = new String(Files.readAllBytes(Paths.get("input1.txt")));
		System.out.println(InverseCaptcha.getSum(captcha));
		System.out.println(InverseCaptcha.getHalfwaySum(captcha));
		
		System.out.println("Day 2: Corruption Checksum\n"
				 		 + "--------------------------");
		String spreadsheet = new String(Files.readAllBytes(Paths.get("input2.txt")));
		System.out.println(CorruptionChecksum.getChecksum(spreadsheet));
		System.out.println(CorruptionChecksum.getEvenDivisorSum(spreadsheet));
		
		System.out.println("Day 3: Spiral Memory\n"
		 		 		 + "--------------------");
		int input3 = 312051;
		System.out.println(SpiralMemory.getSteps(input3));
		
		System.out.println("Day 4: High-Entropy Passphrases\n"
		 		 		 + "-------------------------------");
		String passphrases = new String(Files.readAllBytes(Paths.get("input4.txt")));
		System.out.println(Passphrases.validPassphrases(passphrases));
		System.out.println(Passphrases.validPassphrasesAnagram(passphrases));
		
		System.out.println("Day 5: A Maze of Twisty Trampolines, All Alike\n"
		 		 		 + "----------------------------------------------");
		String offsets = new String(Files.readAllBytes(Paths.get("input5.txt")));
		System.out.println(TrampolineMaze.getSteps(offsets));
	}
}