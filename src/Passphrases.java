// Day 4
public class Passphrases {
	static int validPassphrases(String passphrases) {
		int valid = 0;
		
		String lines[] = passphrases.split("\\r?\\n");
		for (int i = 0; i < lines.length; i++) {
			String words[] = lines[i].split("\\s+");
			
			boolean isValid = true;
			for (int j = 0; j < words.length; j++) {
				for (int k = j+1; k < words.length; k++) {
					if (words[j].equals(words[k])) {
						isValid = false;
					}
				}
			}
			if (isValid) {
				valid++;
			}
		}
		
		return valid;
	}
	
	static int validPassphrasesAnagram(String passphrases) {
		int valid = 0;
		
		String lines[] = passphrases.split("\n");
		for (int i = 0; i < lines.length; i++) {
			String words[] = lines[i].split("\\s+");
			
			boolean isValid = true;
			for (int j = 0; j < words.length; j++) {
				for (int k = j+1; k < words.length; k++) {
					if (words[j].equals(words[k])) {
						isValid = false;
					}
					if (isAnagram(words[j], words[k])) {
						isValid = false;
					}
				}
			}
			if (isValid) {
				valid++;
			}
		}
		
		return valid;
	}
	
	static boolean isAnagram(String word1, String word2) {
		if (word1.length() != word2.length()) {
			return false;
		}
		
		String[] splitWord1 = word1.split("(?!^)");
		String[] splitWord2 = word2.split("(?!^)");
		
		for (int i = 0; i < splitWord1.length; i++) {
			boolean isInWord2 = false;
			for (int j = 0; j < splitWord2.length; j++) {
				if (splitWord1[i].equals(splitWord2[j])) {
					isInWord2 = true;
				}
			}
			
			if (!isInWord2) {
				return false;
			}
		}
		
		return true;
	}
}
